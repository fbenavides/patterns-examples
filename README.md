# README #

Examples with Java of design patterns 

### What is this repository for? ###

* Examples with Java of design patterns
* Version

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

Singleton:
http://www.newthinktank.com/2012/09/singleton-design-pattern-tutorial/

Factory:
http://www.oodesign.com/factory-pattern.html
https://www.youtube.com/watch?v=ub0DXaeV6hA&index=5&list=PLF206E906175C7E07
http://www.newthinktank.com/2012/09/factory-design-pattern-tutorial/

Command:
http://www.oodesign.com/command-pattern.html